create table animal (
                        ID INT NOT NULL IDENTITY PRIMARY KEY,
                        NAME VARCHAR(255),
                        SIZE INT,
                        TYPE VARCHAR(255)
);