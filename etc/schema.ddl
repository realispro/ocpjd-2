create table animal (
                        ID INT AUTO_INCREMENT PRIMARY KEY,
                        NAME VARCHAR(255),
                        SIZE INT,
                        TYPE VARCHAR(255)
);
