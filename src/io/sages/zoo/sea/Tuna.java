package io.sages.zoo.sea;

import io.sages.zoo.Fish;

public class Tuna extends Fish {

    public Tuna(String name, int size) {
        super(name, size);
    }
}
