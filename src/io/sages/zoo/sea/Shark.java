package io.sages.zoo.sea;

import io.sages.zoo.Fish;

public class Shark extends Fish {
    public Shark(String name, int size) {
        super(name, size);
    }
}
