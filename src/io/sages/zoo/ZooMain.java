package io.sages.zoo;

import io.sages.zoo.sea.Shark;
import io.sages.zoo.sea.Tuna;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ZooMain {

    public static void main(String[] args) {

        Eagle bielik = new Eagle("Bielik", 15);
        Kiwi stefan = new Kiwi("Stefan", 9);
        Shark george = new Shark("George", 123);
        Tuna nemo = new Tuna("Nemo", 99);

        List<Animal> animals = new ArrayList<>(List.of(bielik, stefan, george, nemo));

                /*new ArrayList<>(){
            {
                add(bielik);
                add(stefan);
                add(george);
                add(nemo);
                add(new Eagle("Bielik", 15));
            }
        };*/


        System.out.println("animals count:" + animals.size());

        // ********  predicate ***********

        Predicate<Animal> predicate = a -> a.name.equals("Stefan");
        /*new Predicate<Animal>() {
            @Override
            public boolean test(Animal animal) {
                return animal.name.equals("Stefan");
            }
        };*/
        animals.removeIf(a -> a.name.equals("Stefan"));
        /*Iterator<Animal> iterator = animals.iterator();
        while(iterator.hasNext()){
            Animal animal = iterator.next();
            if(animal.name.equals("Stefan")){
                iterator.remove();
            }
        }*/

        // ************ comparator ************

        Comparator<Animal> animalComparator = (a1, a2) -> a1.size - a2.size;
                /*new Comparator<Animal>() {
                    @Override
                    public int compare(Animal a1, Animal a2) {
                        return a1.size-a2.size;
                    }
                };*/
        animals.sort((a1, a2) -> a1.size-a2.size);
        //Collections.sort(animals, (a1, a2) -> a1.size-a2.size);

        // ************** consumer ************

        Consumer<Animal> consumer = System.out::println;
                //x -> System.out.println(x);


                /*new Consumer<Animal>() {
            @Override
            public void accept(Animal animal) {
                System.out.println(animal);
            }
        };*/
        animals.forEach(consumer);
        /*for(var animal : animals){
            System.out.println("animal = " + animal);
        }*/


        Set<Animal> animalSet = new TreeSet<>( (a1, a2) -> a1.size - a2.size);
        animalSet.add(bielik);
        animalSet.add(stefan);
        animalSet.add(george);
        animalSet.add(nemo);

        for(var animal : animalSet){
            System.out.println("[set] animal = " + animal);
        }

        Map<Integer, Animal> habitats = new HashMap<>();
        habitats.put(1, bielik);
        habitats.put(2, stefan);
        habitats.put(3, george);
        habitats.put(4, bielik);
        Animal previous = habitats.put(2, nemo);
        System.out.println("previous = " + previous);

        Animal animal = habitats.get(5);
        System.out.println("animal[2] = " + animal);


        for(Map.Entry<Integer, Animal> entry : habitats.entrySet()){
            System.out.println("key: " + entry.getKey() + ", value: " + entry.getValue());
        }

        // *** inner class ***

        Oyster monika = new Oyster("Monika", 1);

        Oyster.Pearl pearl = new Oyster.Pearl();

        pearl.grow();
        pearl.grow();
        System.out.println("pearl: " + pearl.grow());


        // Local class
        class Foo {

        }

        Foo foo = new Foo();







    }
}
