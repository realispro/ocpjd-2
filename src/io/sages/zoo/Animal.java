package io.sages.zoo;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public abstract class Animal implements Serializable {

    @Serial
    private static final long serialVersionUID = 4790960010037185968L;

    protected transient String name;
    protected int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }


    public void eat(String food){
        System.out.println(name + " is eating " + food);
    }

    public abstract void move();

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    /* @Override
    public int compareTo(Animal o) {
        return o.size - this.size;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return size == animal.size && name.equals(animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
