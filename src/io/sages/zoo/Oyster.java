package io.sages.zoo;

public class Oyster extends Fish{

    private static String staticResource = "staticResource";

    private String resource = "resource";

    public Oyster(String name, int size) {
        super(name, size);
        resource = name;
    }


    public static class Pearl {

        private String mass = "sand";

        public String grow(){
            mass = "[" + mass + "] " + staticResource;
            return mass;
        }
    }

}
