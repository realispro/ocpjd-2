package io.sages.zoo;

import io.sages.zoo.sea.Shark;
import io.sages.zoo.sea.Tuna;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamMain {

    public static void main(String[] args) {
        System.out.println("Let's stream!");

        Eagle bielik = new Eagle("Bielik", 15);
        Kiwi stefan = new Kiwi("Stefan", 9);
        Shark george = new Shark("George", 123);
        Tuna nemo = new Tuna("Nemo", 99);
        Eagle bielik2 = new Eagle("Bielik", 15);

        List<Animal> animals = new ArrayList<>(List.of(bielik, stefan, george, nemo, bielik2));

        // Fork-Join framework
        Stream<Animal> stream = animals.parallelStream();

        if(!stream.isParallel()){
            stream = stream.parallel();
        }

        var result =
            stream
                    // intermediate ops
                    //.filter( a -> a.name.equals("ABC"))
                    .sorted((a1,a2)->a2.size- a1.size)
                    .distinct()
                    .peek(a-> System.out.println("a = " + a))
                    .map( a-> a.name)
                    // terminal ops
                    //.count();
                    //.forEach(a-> System.out.println("foreach: " + a));
                    //.toList(); //JAVA16 .collect(Collectors.toList());
                    //.noneMatch( a -> a.size>100);
                    .findFirst();

        //Supplier<RuntimeException> supplier = () -> new IllegalStateException("should be present");
        System.out.println("result = " + result.orElse(null));

        result.ifPresentOrElse(
                a-> System.out.println("animal present: " + a),
                () -> System.out.println("missing animal :(")
        );

        Stream<Integer> integerStream =
                Stream.iterate(1, i -> ++i );
                //Stream.generate(() -> 102);

        integerStream
                .limit(10)
                .forEach(i-> System.out.println(i));

        IntStream intStream = IntStream.iterate(1, i -> ++i);

        OptionalDouble average = intStream.limit(10).average();

    }
}
