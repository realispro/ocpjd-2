package io.sages.calc;

public interface Memory {

    public Double getValue();

    public void setValue(Double value);

    public Double[] getValues();

}
