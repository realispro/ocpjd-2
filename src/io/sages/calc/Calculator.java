package io.sages.calc;

import io.sages.calc.memory.ArrayMemory;

public abstract class Calculator {

    static {
        System.out.println("[Calculator] static block");
    }

    protected Memory memory = new ArrayMemory(5);

    {
        System.out.println("[Calculator] initialization block");
    }

    public Calculator(double result){
        super();
        // initialization block here
        memory.setValue(result);
        System.out.println("[Calculator] parametrized constructor: " + result);
    }

    public Calculator(){
        this(0);
        System.out.println("[Calculator] default constructor");
    }

    public double add(double operand){
        memory.setValue(memory.getValue()+operand);
        return memory.getValue();
    }

    public double subtract(double operand){
        memory.setValue(memory.getValue()-operand);
        return memory.getValue();
    }

    public double multiply(double operand){
        memory.setValue(memory.getValue()*operand);
        return memory.getValue();
    }

    public double divide(double operand) throws CalculatorException {
        memory.setValue(memory.getValue()/operand);
        return memory.getValue();
    }

    public abstract void display();















    public static int multiplyAll(int... operands){
        System.out.println("multiplyAll on Calculator");
        int result = 1;
        for(var operand : operands){
            result *= operand;
        }
        return result;
    }

    public static void foo(Object... objects){
        for(var o : objects){
            System.out.println("o = " + o);
        }
    }
}
