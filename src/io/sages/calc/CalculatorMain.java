package io.sages.calc;

public class CalculatorMain {


    public static void main(String[] args) {
        System.out.println("Let's calculate!");

        Calculator c1 = new ScientificCalculator();
        Calculator c2 = new ScientificCalculator();

        c1.toString();
        c1.add(2);
        c1.add(2);
        c1.multiply(3);

        try {
            c1.divide(0);
        } catch ( Exception e) {
            e.printStackTrace();
            //System.exit(1234);
        } finally {
            System.out.println("in finally block");
        }

        if(c1 instanceof ScientificCalculator sc) {
            //ScientificCalculator sc = (ScientificCalculator) c1;
            sc.power(8);
        }
        c1.subtract(250);

        c1.display();

        //describe((ScientificCalculator) c1);
    }



    public static void describe(Calculator c){
        System.out.println("probably cheap device");
    }

    public static void describe(ScientificCalculator c){
        System.out.println("scientific device");
    }

}
