package io.sages.calc.memory;

import io.sages.calc.Memory;

import java.io.Serializable;

public interface FooMemory extends Memory, Serializable {

    public static final int foo = 1;

    // Java 8
    static String remember(){
        return "bar foo" + doSomething();
    }

    // Java 9
    private static String doSomething(){
        return "something";
    }

    public abstract void bar();

    // JAVA 8
    default void describe(){
        System.out.println("foo bar" + doSomething());
    }

}
