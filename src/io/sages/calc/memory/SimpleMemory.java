package io.sages.calc.memory;

import io.sages.calc.Memory;

public class SimpleMemory implements Memory {

    private double value;

    @Override
    public Double getValue() {
        return value;
    }

    @Override
    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public Double[] getValues() {
        return new Double[]{value};
    }

    @Override
    public String toString() {
        return "SimpleMemory{" +
                "value=" + value +
                '}';
    }
}
