package io.sages.calc.memory;

import io.sages.calc.Memory;

public class ArrayMemory implements Memory {

    // [null, null, null, null, null]
    private Double[] values;

    private int index = -1;

    public ArrayMemory(int memorySize){
        values = new Double[memorySize];
    }

    @Override
    public Double getValue() {
        return values[index];
    }

    @Override
    public void setValue(Double value) {
        //v1: [0,1,2,3,4] + 5 -> [1,2,3,4,5] i:4
        //v2: [0,1,2,3,4] + 5 -> [5,1,2,3,4] i:0
        if(index<(values.length-1)) {
            index++;
        } else {
            System.arraycopy(values, 1, values, 0, values.length-1);
        }
        values[index] = value;

    }

    @Override
    public Double[] getValues() {
        return values;
    }

    @Override
    public String toString() {

        //String valuesString = "[";
        StringBuilder builder = new StringBuilder("[");

        for(var value : this.values){
            //valuesString = valuesString + value + ", ";
            builder.append(value).append(", ");
        }
        //valuesString+="]";
        builder.append("]");

        return "ArrayMemory{" +
                "values=" + builder.toString() +
                ", index=" + index +
                '}';
    }
}
