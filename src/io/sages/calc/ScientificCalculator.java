package io.sages.calc;

public class ScientificCalculator extends Calculator {

    static {
        System.out.println("[ScientificCalculator] static block");
    }

    {
        System.out.println("[ScientificCalculator] initialization block");
    }

    public ScientificCalculator(){
        super(0);
        // initialization block here
        System.out.println("[ScientificCalculator] default constructor");
    }


    public double power(int operand){
        memory.setValue(Math.pow(memory.getValue(), operand));
        return memory.getValue();
    }

    @Override
    public double divide(double operand) throws CalculatorException {
        if(operand==0){
            System.out.println("WARNING pamietaj cholero nie dziel przez zero");
            throw new CalculatorException("nie dziel przez zero!");
        }
        return super.divide(operand);
    }

    @Override
    public void display() {
        System.out.println("[ScientificCalculator] result: " + memory);
    }

    public static int multiplyAll(int... operands){
        System.out.println("multiplyAll on ScientificCalculator");
        int result = 1;
        for(var operand : operands){
            result *= operand;
        }
        return result;
    }
}
