package io.sages.concurrency.bank;

import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger amount;

    //private Lock lock = new ReentrantLock();
    //private Object lock = new Object();

    public Account(int amount) {
        this.amount = new AtomicInteger(amount);
    }

    public void deposit(int value){

        //lock.lock();
        amount.addAndGet(value);
        //amount += value; // amount = amount + value
        //lock.unlock();

    }

    public void withdraw(int value){
        //lock.lock();
        amount.addAndGet(-value);
        //amount -= value; // amount = amount - value
        //lock.unlock();
    }

    public int getAmount() {
        return amount.get();
    }
}
