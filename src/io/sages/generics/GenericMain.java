package io.sages.generics;

public class GenericMain {

    public static void main(String[] args) {
        System.out.println("Let's generalize!");

        Box<Integer> box = Box.create(123);
                //new IntegerBox();
        //box.put(123);

        int i = box.get();

        System.out.println("i = " + i);

    }


    public static <E> Box<E> create(E content){

        Box<E> box = new Box<>();
        box.put(content);
        return box;
    }

}
