package io.sages.generics;

public class Box<T> {

    private T content;

    public void put(T content){
        this.content = content;
    }

    public T get(){
        return content;
    }

    public static <E> Box<E> create(E content){

        Box<E> box = new Box<>();
        box.put(content);
        return box;
    }

    public static <E> Box<E> create(){

        Box<E> box = new Box<>();
        return box;
    }


}
