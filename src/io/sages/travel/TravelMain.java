package io.sages.travel;

public class TravelMain {

    public static void main(String[] args) {
        System.out.println("Let's travel!");

        String traveller = "Jan Kowalski";

        Transportation transportation = getTransportation();
        System.out.println("transportation speed: " + transportation.getSpeed());
        transportation.transport(traveller);

    }


    public static Transportation getTransportation(){
        return //p -> System.out.println("passenger " + p + " is being teleported.");
           new Plane();
    }
}
