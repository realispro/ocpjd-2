package io.sages.travel;

@FunctionalInterface
public interface Transportation {


    void transport(String passenger);

    default int getSpeed(){
        return -1;
    }

}
