package io.sages.travel;

public class Plane implements Transportation{
    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is being transported by plane.");
    }

    @Override
    public int getSpeed() {
        return 1000;
    }
}
