package io.sages;

import io.sages.zoo.Animal;
import io.sages.zoo.Kiwi;

import java.sql.*;

public class JdbcMain {

    public static void main(String[] args) {

        Animal animal = new Kiwi("Stefan", 5);

        try(Connection connection = getConnection()){

            connection.setAutoCommit(false);

            System.out.println("db version: " + connection.getMetaData().getDatabaseProductVersion());

            PreparedStatement statement = connection.prepareStatement(
                    "insert into animal (name, size, type) values (?,?,?)");
            statement.setString(1, animal.getName());
            statement.setInt(2, animal.getSize());
            statement.setString(3, animal.getClass().getName());

            statement.addBatch();
            //statement.executeUpdate();

            statement.setString(1, animal.getName() + animal.getName());
            statement.setInt(2, animal.getSize()*2);
            //statement.setString(3, animal.getClass().getName());

            statement.addBatch();
            //statement.executeUpdate();

            statement.executeBatch();

            connection.commit();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        try(Connection connection = getConnection()){

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select name, size, type from animal");

            while (rs.next()){

                String name = rs.getString(1);
                int size = rs.getInt("size");
                String type = rs.getString("type");

                System.out.println("animal of type " + type + " and name " + name + " and size " + size);

            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public static Connection getConnection(){

        String url = "jdbc:mysql://localhost:3306/lab";
        String user = "root";
        String password = "mysql";

        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

}
