package io.sages;

public enum WeekDay {

    MONDAY(false),
    TUESDAY(false),
    WEDNESDAY(false),
    THURSDAY(false),
    FRIDAY(true),
    SATURDAY(true),
    SUNDAY(true);

    private boolean weekend;

    WeekDay(boolean weekend){
        this.weekend = weekend;
    }

    public boolean isWeekend() {
        return weekend;
    }
}
