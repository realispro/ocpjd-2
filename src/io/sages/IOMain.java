package io.sages;

import io.sages.zoo.Animal;
import io.sages.zoo.Kiwi;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class IOMain {

    public static void main(String[] args) {

        // try-with-resources
        try(BufferedWriter bw = new BufferedWriter(new FileWriter( getNIOFile(), true))){

            bw.write(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + "\n");

        } catch (IOException e){
            e.printStackTrace();
        } /*finally {
            try {
                if(bw!=null) {
                    bw.close();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }*/

        try(BufferedReader br = new BufferedReader(new FileReader(getNIOFile()))){

            br.lines().forEach(System.out::println);

            /*String line;

            while( (line = br.readLine() )!=null  ){
                System.out.println(line);
                        //DateTimeFormatter.ISO_DATE_TIME.parse(line));
            }*/

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Animal animal = new Kiwi("Stefan", 5);

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("mydir\\animal.dat"))){
            oos.writeObject(animal);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("mydir\\animal.dat"))){
            animal = (Animal) ois.readObject();
            System.out.println("animal = " + animal);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }


    }

    public static File getNIOFile() throws IOException {

        Path pathDir = Paths.get("mydir");
        if(Files.exists(pathDir)){
            System.out.println("directory already exists");
        } else {
            System.out.println("directory is missing. creating one...");
            Files.createDirectories(pathDir);
        }
        Path pathFile = Paths.get("mydir", "myfile.txt");
        if(Files.exists(pathFile)){
            System.out.println("file already exists");
        } else {
            System.out.println("file is missing. creating one...");
            Files.createFile(pathFile);
        }

        return pathFile.toFile();
    }


    public static File getIOFile() throws IOException {
        File dir = new File("mydir\\..\\mydir");

        if(dir.exists()){
            System.out.println("directory already exists");
        } else {
            System.out.println("directory is missing. creating one...");
            dir.mkdirs();
        }

        File file = new File( dir,"myfile.txt");
        if(file.exists()){
            System.out.println("file already exists");
        } else {
            System.out.println("file is missing. creating one...");
            file.createNewFile();
        }
        return file;
    }
}
