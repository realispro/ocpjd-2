package io.sages;

import io.sages.calc.Calculator;
import io.sages.calc.memory.FooMemory;

import java.util.Arrays;

import static io.sages.calc.Calculator.multiplyAll;

public class HelloWorld {


    static public void main(String[] args){
        System.out.println("Let's learn!");


        var weekDay = 2147483648L;
        System.out.println("weekDay = " + weekDay);

        double price = 1.1;
        System.out.println("price = " + price);

        boolean weekend = false;
        System.out.println("weekend = " + weekend);

        char c = 'H';
        System.out.println("c = " + (short)c);

        Integer i1 = 127;
        Integer i2 = 127;

        System.out.println("same: " + (i1==i2));
        System.out.println("equals: " + i1.equals(i2));

        
        int[] ints = /*new int[]*/{0,1,2,3,4};
        /*ints[0] = 0;
        ints[1] = 1;
        ints[2] = 2;
        ints[3] = 3;
        ints[4] = 4;*/

        System.out.println("Arrays.toString(ints) = " + Arrays.toString(ints));

        for(int i=0; i<ints.length; i++){
            System.out.println("ints[" + i + "] = " + ints[i]);
        }

        for( int i : ints){
            System.out.println("ints[next] = " + i);
        }




        int z = 1;
        final int one = 1;
        switch(z){ // int, short, byte, char, String, enum
            default:
                System.out.println("other");
            case one:
                System.out.println("one");
                break;
            case 2:
                System.out.println("two");
                break;
        }

        String season = "winter";
        String translation = switch(season){
            case "winter":
                yield "Zima";
            default:
                yield "nie zima";
        };

        translation = switch(season){
            case "winter" -> "Zima";
            default -> "nie zima";
        };


        boolean condition = true;

        /*do
            System.out.println("continuing...");
        while (condition);*/

        boolean myVal = false;
        if (myVal = true)
            for (int i = 0; i < 2; i++)
                System.out.println(i);
        else
            System.out.println("else");

        int myField = 1;
        myField = myField++ + --myField; //   1 + 0 = 1
        //  1 + 1

        System.out.println("myField = " + myField);

        int _1 = 1;
        System.out.println("_1 = " + _1);

        int size = 15;
        int[][] results = new int[size][size];

        OUTER:
        for(int x=1; x<=size; x++){
            for(int y=1; y<=size; y++){
                if(y==13 || x==13){
                    break OUTER;
                }
                results[x-1][y-1] = multiplyAll(x, y, 3, 5);
            }
        }


        for( int[] row : results) {
            for( int i : row){
                System.out.print(i + "\t");
            }
            System.out.println();
        }


        Calculator.foo( (Object)new Object[]{1, 2} );

        FooMemory fooMemory = null;

        FooMemory.remember();

        WeekDay wd = WeekDay.SATURDAY;
        System.out.println("wd = " + wd.isWeekend());

        String s1 = new String("World Cup");
        s1 = s1.intern();
        String s2 = "World Cup";

        System.out.println("same: " + (s1==s2) );
        System.out.println("equals: " + s1.equals(s2));


        int x = 5;
        int y = 8;
        System.out.println(x + y + " result " + x + y);




    }

/*    public static int multiply(int x, int y){
        return x * y;
    }*/


}
