package io.sages;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringMain {

    public static void main(String[] args) {

        Scanner s = new Scanner("Each month in 2008 has 30.5 days on average.");
        while (s.hasNext()) {
            if (s.hasNextInt()) {
                System.out.print(" INT: " + s.nextInt());
            } else if (s.hasNextDouble()) {
                System.out.print(" DOUBLE: " + s.nextDouble());
            } else {
                System.out.print(" OTHER: " + s.next());
            }
        }

        System.out.println();


        // regex

        String text = "00-950 Warszawa, 30123 Krakow, 88 123 Torun";
        String patternText = "\\d{2}[-\\s]?\\d{3}";

        Pattern p = Pattern.compile(patternText);
        Matcher m = p.matcher(text);

        while(m.find()){
            System.out.println(m.start() + "-" + m.end() + ":" + m.group());
        }


    }
}
