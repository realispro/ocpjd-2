package io.sages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateMain {

    public static void main(String[] args) {

        Calendar.getAvailableCalendarTypes().forEach(System.out::println);
        
        // legacy 
        Date date = new Date(); // now in millis

        Calendar calendar = Calendar.getInstance();
        System.out.println("week date supported: " + calendar.isWeekDateSupported());

        calendar.set(2022, Calendar.DECEMBER, 31, 23, 59, 59);
        calendar.roll(Calendar.DATE, 1000);

        date = calendar.getTime();

        Locale locale = new Locale("pl", "PL");
                //Locale.getDefault();

        DateFormat format = new SimpleDateFormat("yy YY", locale);
                //DateFormat.getDateInstance(DateFormat.FULL, locale);
                //.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);

        String dateString = format.format(date);
        System.out.println("dateString = " + dateString);
        
        // java.time
        LocalDate ld = LocalDate.now();
        LocalTime lt = LocalTime.now();
        LocalDateTime christmasEve = LocalDateTime.of(2022, Month.DECEMBER, 24, 16, 0);
        LocalDateTime now = LocalDateTime.now();

        Duration duration = Duration.between(now, christmasEve);

        System.out.println("duration = " + ChronoUnit.NANOS.between(now, christmasEve));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy$MM$dd E", locale);

        dateString = ld.format(formatter);
                //formatter.format(ld);
        System.out.println("[java.time] dateString = " + dateString);


        ZonedDateTime warsawNow = now.atZone(ZoneId.of("Europe/Warsaw"));
        ZonedDateTime warsawLanding = warsawNow.plusHours(7);
        ZonedDateTime singaporeLanding = warsawLanding.withZoneSameInstant(ZoneId.of("Asia/Singapore"));

        DateTimeFormatter zoneFormatter = DateTimeFormatter.ofPattern("yyyy$MM$dd hh$mm X");
        System.out.println("starting: " + warsawNow.format(zoneFormatter) + ", landing: " + singaporeLanding.format(zoneFormatter));

        LocalDateTime d = LocalDateTime.of(2015, 5, 10, 11, 22, 33);
        Period p = Period
                .ofDays(1)
                .plusYears(2);
                //.ofYears(2);
        d = d.minus(p); /*  w  w w. j  ava  2  s .  c  om*/
        DateTimeFormatter f = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        System.out.println(f.format(d));
    }
}
